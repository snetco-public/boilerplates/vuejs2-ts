import axios from 'axios'

const urlString = window.location.href
const url = new URL(urlString)
const appIframeName = JSON.parse(
  decodeURIComponent(url.search)
    .slice(1)
    .replace(/\+/g, ' ')
    .split('&')[0]
)
const token = appIframeName.token
export const sectorId = appIframeName.sectorId
export const businessSector = appIframeName.businessSector

export default (axiosOptions: any = {}, withoutAuthorization: any = false) => {
  const defaultOptions = {
    baseURL: process.env.VUE_APP_BASE_URL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
      'Sector-id': sectorId,
    },
  }

  const options = { ...defaultOptions, ...axiosOptions }
  return axios.create(options)
}
