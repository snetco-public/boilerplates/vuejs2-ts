import { Vue, Component, Watch } from 'vue-property-decorator'
import _ from 'lodash'

@Component({})
export default class Table extends Vue {
  data: any = []
  items: any = []
  totalRecords = 0
  loading = true
  pagination: any = {}
  search = ''

  public get pages() {
    if (!this.pagination.itemsPerPage || !this.totalRecords) return 0

    return Math.ceil(this.totalRecords / this.pagination.itemsPerPage)
  }

  @Watch('pagination', { deep: true })
  onPaginationChange() {
    // @ts-ignore
    this.fetchData()
  }
  @Watch('search', { deep: true })
  onSearchChange() {
    // @ts-ignore
    const that = this
    _.debounce(function() {
      that.pagination.page = 1
      // @ts-ignore
      this.fetchData()
    }, 500)
  }

  prepareQuery(params?: any) {
    const { page, sortBy, sortDesc, itemsPerPage } = this.pagination
    let extraParams = ''
    for (const paramName in params) {
      extraParams += `&${paramName}=${params[paramName]}`
    }
    const query = `?page-size=${itemsPerPage}&page=${page}&sort-by=${sortBy}&descending=${sortDesc[0]}`
    if (this.search) {
      extraParams += `&qs=${this.search}`
    }
    return `${query}${extraParams}`
  }
  statusChanged() {
    // @ts-ignore
    this.fetchData()
  }
}
