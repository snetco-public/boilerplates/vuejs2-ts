import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '@/views/Accounts/Views/Index.vue'
import Clients from '@/views/Clients/Views/ClientView.vue'
import BanksIndex from '@/views/Banks/views/Index.vue'
import ClientReceipts from '@/views/Receipts/views/ClientReceipts.vue'
import BankDiaryDataTable from '../views/Banks/components/BankDiaryDataTable.vue'
Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/accounts',
    name: 'Accounts',
    component: Home,
  },
  {
    path: '/banks',
    name: 'BanksIndex',
    component: BanksIndex,
  },
  {
    path: '/bank-history',
    name: 'BankDiaryDataTable',
    component: BankDiaryDataTable,
    props: true,
  },
  {
    path: '/clients',
    name: 'Clients',
    component: Clients,
  },
  {
    path: '/clients/receipts',
    name: 'ClientReceipts',
    component: ClientReceipts,
  },
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
})

export default router
